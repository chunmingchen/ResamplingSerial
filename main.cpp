
#include <cstdio>
#include <iostream>
#include <vector>
#include <map>
#include <iomanip>

#include "vtkSmartPointer.h"
#include "vtkRenderWindowInteractor.h"
#include "vtkInteractorStyleTrackballCamera.h"
#include "vtkObjectFactory.h"
#include "vtkXMLPolyDataReader.h"
#include "vtkAxes.h"
#include "vtkAxesActor.h"
#include "vtkImageData.h"
#include <vtkExtractEdges.h>
#include "vtkVector.h"
#include "vtkTriangle.h"
#include "vtkTriangleFilter.h"
#include "vtkPoints.h"
#include "vtkCellArray.h"
#include "vtkQuadricClustering.h"
#include "vtkPLYReader.h"
#include "vtkPolyDataWriter.h"
#include "vtkQuadric.h"
#include "vtkPolyDataReader.h"
#include "vtkSmartPointer.h"
#include "vtkDataSetMapper.h"
#include "vtkOutlineFilter.h"
#include "vtkThreshold.h"

#include "vtkExodusIIReader.h"
#include "vtkStructuredGridReader.h"

#include <vtkProperty.h>
#include "vtkMultiBlockPLOT3DReader.h"
#include "vtkAppendFilter.h"
#include "vtkUnstructuredGrid.h"
#include "vtkMultiBlockDataSet.h"
#include "vtkStructuredGrid.h"
#include <vtkRectilinearGrid.h>
#include <vtkPointData.h>
#include <vtkCellData.h>
#include <vtkFloatArray.h>
#include <vtkDoubleArray.h>
#include <vtkXMLImageDataWriter.h>
#include <vtkDataSetAttributes.h>
#include <vtkDataObject.h>

#include "vtkImageProbeFilter.h"
#include "vtkProbeFilter.h"
#include "vtkDataSetTriangleFilter.h"

#include "vtkPolyDataMapper.h"
#include "vtkRenderWindow.h"
#include "vtkActor.h"
#include "vtkRenderer.h"
#include "vtkMatrix4x4.h"
#include "vtkMatrix3x3.h"

#include "cp_time.h"

using namespace std;
//using namespace boost::numeric;

#define vsp_new(type, name) vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

vtkRenderer *ren;
vtkRenderWindow *renWin;

bool bShowGrids = false;
#ifdef PROFILING
bool bShowOriginal = false;
#else
bool bShowOriginal = true;
#endif
bool bShowOutput = false;
bool bUseVTKSimplify = false;

/// for mesh simplification
vtkSmartPointer<vtkUnstructuredGrid> data;
vtkSmartPointer<vtkImageData> output_data;

double origin[3];
int xdim, ydim, zdim;
void set_origin_and_dim();
double grid_width ;
int grid_division = 30;
int bound_scale = 1;

/// determine grid resolution for clustering
void set_origin_and_dim()
{
    double bounds[6];
    data->GetBounds(bounds);

    // scale bounds
    bounds[1] = bounds[0] + (bounds[1]-bounds[0])*bound_scale;
    bounds[3] = bounds[2] + (bounds[3]-bounds[2])*bound_scale;
    bounds[5] = bounds[4] + (bounds[5]-bounds[4])*bound_scale;

    double res[3];
    for (int i=0; i<3; i++)
        res[i] = (bounds[i*2+1]-bounds[i*2])/grid_division;
    grid_width = std::max(res[0], std::max(res[1], res[2]));


    double one_over_grid_width = 1. / grid_width;

    xdim = min((int)ceil((bounds[1]-bounds[0])*one_over_grid_width)+1, grid_division);
    ydim = min((int)ceil((bounds[3]-bounds[2])*one_over_grid_width)+1, grid_division);
    zdim = min((int)ceil((bounds[5]-bounds[4])*one_over_grid_width)+1, grid_division);
    if (1) {
        origin[0] = bounds[0];
        origin[1] = bounds[2];
        origin[2] = bounds[4];
    }else {
        origin[0] = (bounds[1]+bounds[0])*0.5 - grid_width*(xdim)*.5;
        origin[1] = (bounds[3]+bounds[2])*0.5 - grid_width*(ydim)*.5;
        origin[2] = (bounds[5]+bounds[4])*0.5 - grid_width*(zdim)*.5;
    }
    printf("Dim: %d %d %d\n", xdim, ydim, zdim);
}

void draw()
{
    ren->RemoveAllViewProps();

    // draw data
    if (bShowOriginal)
    {
      vsp_new(vtkDataSetMapper, mapper);
      mapper->SetInputData( data );

      vsp_new(vtkActor, polyactor);
      polyactor->SetMapper(mapper);
      polyactor->GetProperty()->SetRepresentationToSurface();

      ren->AddActor(polyactor);
    }

    // draw data
    if (bShowOutput)
    {
      {
        // outline
        vsp_new(vtkOutlineFilter, outline);
        outline->SetInputData( output_data );
        outline->Update();

        vsp_new(vtkPolyDataMapper, mapper);
        mapper->SetInputData( outline->GetOutput() );

        vsp_new(vtkActor, polyactor);
        polyactor->SetMapper(mapper);
        polyactor->GetProperty()->SetColor(1,1,0);

        ren->AddActor(polyactor);
      }

      // transparent thresholds
      {
        vsp_new(vtkThreshold, threshold);
        threshold->SetInputData(output_data);
        threshold->SetInputArrayToProcess(0, 0, 0, vtkDataObject::FIELD_ASSOCIATION_POINTS, "vtkValidPointMask");
        threshold->ThresholdByUpper(0.5);
        threshold->Update();

        vsp_new(vtkDataSetMapper, mapper);
        mapper->SetInputData( threshold->GetOutput() );

        vsp_new(vtkActor, actor);
        actor->SetMapper(mapper);
        actor->GetProperty()->SetRepresentationToSurface();
        actor->GetProperty()->SetOpacity(.5);

        ren->AddActor(actor);
      }
    }


    // axes
    vsp_new(vtkAxesActor, axes);
    ren->AddActor(axes);

    if (bShowGrids)
    {
        cout << "grid width=" << grid_width << endl;
        cout << "grid_division = " << grid_division << endl;
        set_origin_and_dim();

        vsp_new(vtkImageData, image);
        image->SetDimensions(xdim+1, ydim+1, zdim+1); // grids are larger than cells by one in each dimension
        image->SetOrigin(origin[0], origin[1], origin[2]);
        image->SetSpacing(grid_width, grid_width, grid_width);

        vsp_new(vtkDataSetMapper, mapper);
        mapper->SetInputData(image);

        vsp_new(vtkActor, polyactor);
        polyactor->SetMapper(mapper);
        polyactor->GetProperty()->SetRepresentationToWireframe();

        ren->AddActor(polyactor);
    }

    ren->SetBackground(0,0,.5); // Background color
    renWin->Render();
}

// Define interaction style
class KeyPressInteractorStyle : public vtkInteractorStyleTrackballCamera
{
  public:
    static KeyPressInteractorStyle* New();
    vtkTypeMacro(KeyPressInteractorStyle, vtkInteractorStyleTrackballCamera);

    virtual void OnKeyPress()
    {
      // Get the keypress
      vtkRenderWindowInteractor *rwi = this->Interactor;
      std::string key = rwi->GetKeySym();

      // Output the key that was pressed
      std::cout << "Pressed " << key << std::endl;

      // Handle an arrow key
      if(key == "Up")
        {
        std::cout << "The up arrow was pressed." << std::endl;
        }

      // Handle a "normal" key
      if(key == "s")
        {
          cout << "Input number of points: " << data->GetNumberOfPoints() << ", number of cells: " << data->GetNumberOfCells() << endl;
          set_origin_and_dim();

          // create grids
          vsp_new(vtkImageData, image);
          image->SetDimensions(xdim, ydim, zdim); // grids are larger than cells by one in each dimension
          image->SetOrigin(origin[0], origin[1], origin[2]);
          image->SetSpacing(grid_width, grid_width, grid_width);
          image->AllocateScalars(VTK_FLOAT, 1);

          Timer timer;
          timer.start();
          if (bUseVTKSimplify) {
            vsp_new(vtkProbeFilter, probe);
            probe->SetInputData(image);
            probe->SetSourceData(data);
            probe->Update();
            output_data = vtkImageData::SafeDownCast( probe->GetOutput() ) ;
          } else {

            vsp_new(vtkImageProbeFilter, probe);
            probe->SetInputData(image);
            probe->SetSourceData(data);
            probe->Update();
            output_data = vtkImageData::SafeDownCast( probe->GetOutput() ) ;
            if (output_data.Get()==NULL)
              cout << "data not image data" << endl;
          }
          timer.end();
          cout << "Time: " << (double)timer.getElapsedUS()/1000. << " ms" << endl;

          bShowOriginal = false;
          bShowOutput = true;
          draw();
        }
      if (key == "o" )
      {
          bShowOriginal = !bShowOriginal ;
          bShowOutput = !bShowOutput;
          draw();
      }
      if (key=="minus") {
          if (grid_division>1)
            grid_division /= 2;
          cout << "grid division =" << grid_division << endl;
          if (bShowGrids)
            draw();
      }
      if (key=="plus") {
          grid_division *= 2;
          cout << "grid division =" << grid_division << endl;
          if (bShowGrids)
            draw();
      }
      if (key=="asterisk") {
        bound_scale *= 2;
        cout << "bound_scale = " << bound_scale << endl;
        if (bShowGrids)
          draw();
      }
      if (key=="slash") {
        if (bound_scale > 1)
          bound_scale /= 2;
        cout << "bound_scale = " << bound_scale << endl;
        if (bShowGrids)
          draw();
      }
      if (key=="g") {
          bShowGrids = ! bShowGrids;
          draw();
      }
      if (key=="v") {
          bUseVTKSimplify = ! bUseVTKSimplify;
          cout << "Use vtk = " << bUseVTKSimplify << endl;
      }
      if (key=="z") {
          cout << "Saving to output.vti ..." << endl;
          vsp_new(vtkXMLImageDataWriter , writer);
          writer->SetFileName("output.vti");
          writer->SetInputData(output_data);
          writer->Write();
      }

      // Forward events
      vtkInteractorStyleTrackballCamera::OnKeyPress();
    }

};
vtkStandardNewMacro(KeyPressInteractorStyle);

void load_input(const char *filepath)
{
  printf("Loading path: %s\n", filepath);

  // convert to unstructured
  vsp_new(vtkAppendFilter, append);

  if (strstr(filepath, "noinj")) {
      string gfile = string(filepath)+string("/s35_noinj.r2b1.p3d.g49401");
      string qfile = string(filepath)+string("/s35_noinj.r2b1.p3d.q49401");

      vsp_new(vtkMultiBlockPLOT3DReader, reader);
      reader->SetXYZFileName(gfile.c_str());
      reader->SetQFileName(qfile.c_str());
      reader->AddFunction(100);
      reader->AddFunction(110);
      reader->AddFunction(200);
      reader->SetAutoDetectFormat(1);
      reader->SetScalarFunctionNumber(110);
      reader->SetVectorFunctionNumber(200);
      reader->Update();

      append->AddInputData(reader->GetOutput()->GetBlock(0));
    }
  else if (strstr(filepath, "ex2")) {
    vsp_new(vtkExodusIIReader, reader);
    reader->SetFileName(filepath);
    reader->Update();
    append->AddInputData(reader->GetOutput());
  } else {
    vsp_new(vtkStructuredGridReader, reader);
    reader->SetFileName(filepath);
    reader->Update();
    append->AddInputData(reader->GetOutput());
  }


#if 0
  vsp_new(vtkDataSetTriangleFilter, triangulation);
  triangulation->SetInputConnection(append->GetOutputPort());
  triangulation->Update();
  data = vtkSmartPointer<vtkUnstructuredGrid>::New();
  data->DeepCopy(triangulation->GetOutput());
#else
  append->Update();
  data = vtkSmartPointer<vtkUnstructuredGrid>::New();
  data->DeepCopy(append->GetOutput());
#endif


}

int main( int argc, char **argv )
{
    cout << "Input arguments: <vtp/vtk/ply file> <grid_division=30> <max cores>\n";

    if (argc>2)
        grid_division = atoi(argv[2]);

    if (argc>1)
        load_input(argv[1]);
    else
        load_input(DATA_PATH);

    set_origin_and_dim();

#ifdef PROFILING
    benchmark();
    exit(0);
#endif
    printf("Keys:\n"
           "g: Toggle showing grids\n"
           "+/-: Increase/decrease grid size by 2\n"
           "<<< s: Simplify mesh >>>\n"
           "o: Toggle showing orginal model or simplified model\n"
           "v: Toggle using VTK simplification filter\n"
           "z: Save output data\n"
           );

    // Visualize
    vsp_new(vtkRenderer, ren);
    vsp_new(vtkRenderWindow, renWin);
    ::ren = ren.GetPointer();
    ::renWin = renWin.GetPointer();

    renWin->AddRenderer(ren);
    renWin->SetSize(800,600);

    vsp_new(vtkRenderWindowInteractor, renderWindowInteractor );
    renderWindowInteractor->SetRenderWindow(renWin);

    vsp_new(KeyPressInteractorStyle, style);
    style->SetCurrentRenderer(ren);
    renderWindowInteractor->SetInteractorStyle(style);

    draw();

    ren->ResetCamera();

    renWin->Render();

    renderWindowInteractor->Start();
    return EXIT_SUCCESS;

}
